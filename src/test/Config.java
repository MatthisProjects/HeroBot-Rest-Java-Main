package fr.aliveCreations.matthis974jump.botsurveillancediscord.bot_core_system.boot;


import com.google.gson.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Config {
    private MainConfig mainConfig;
    private Map<String,JsonElement> keys = new HashMap<>();
    private JsonParser jsonParser = new JsonParser();
    private Gson gson = new GsonBuilder().create();
    void setMainObjectConfigFromJson(String s) {
        mainConfig = gson.fromJson(s,MainConfig.class);
    }

    void addAnexalConfig(String s) throws BootException {
        JsonObject je = jsonParser.parse(s).getAsJsonObject();
        Set<Map.Entry<String, JsonElement>> entries = je.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            if(!keys.containsKey(entry.getKey())){
                keys.put(entry.getKey(),entry.getValue());
            }else {
                throw new BootException("Duplicate key ! :"+entry.getKey());
            }
        }
    }

    public MainConfig getMainConfig() {
        return mainConfig;
    }

    public Map<String, JsonElement> getKeys() {
        return keys;
    }

    public JsonParser getJsonParser() {
        return jsonParser;
    }

    public Gson getGson() {
        return gson;
    }

    public class MainConfig{
        private List<Bot> botList;

        public List<Bot> getBotList() {
            return botList;
        }

        public class Bot{
            private String name;
            private String token;
            private String game;
            private String prefix;
            public String getName() {
                return name;
            }

            public String getToken() {
                return token;
            }

            public String getGame() {
                return game;
            }

            public String getPrefix() {
                return prefix;
            }

            public void setPrefix(String prefix) {
                this.prefix = prefix;
            }

            private String[] disabledsPlugins;

            public String[] getDisabledsPlugins() {
                return disabledsPlugins;
            }
        }
    }
}
