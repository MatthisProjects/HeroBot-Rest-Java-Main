package fr.matthis974.herobot.core_features.utils;

import fr.matthis974.herobot.BooterService;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.logging.*;

public class Loggable {
    private static ConsoleHandler consoleHandler = new ConsoleHandler();
    private static Formatter handler = new java.util.logging.Formatter() {
        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_RED = "\u001B[31m";
        public static final String ANSI_GREEN = "\u001B[32m";
        public static final String ANSI_YELLOW = "\u001B[33m";
        public static final String ANSI_BLUE = "\u001B[34m";
        public static final String ANSI_CYAN = "\u001B[36m";
        public static final String ANSI_WHITE = "\u001B[37m";
        public static final String ANSI_RED_NAME = "SEVERE";
        public static final String ANSI_YELLOW_NAME = "WARNING";
        public static final String ANSI_GREEN_NAME = "INFO";
        public static final String ANSI_RED_NAME_2 = "OFF";
        private static final String format = ANSI_WHITE+"[%1$tF %1$tT] "+ANSI_BLUE+"[%2$-10s] [%4$s]-s- %3$s %n";

        @Override
        public String format(LogRecord lr) {
            String s = ANSI_RESET;

            switch (lr.getLevel().getName()){
                case ANSI_YELLOW_NAME:
                    s += ANSI_YELLOW;break;
                case ANSI_RED_NAME:
                    s += ANSI_RED;break;
                case ANSI_GREEN_NAME:
                    s += ANSI_GREEN;break;
                case ANSI_RED_NAME_2:
                    s += ANSI_RED;break;
                default:
                    s += ANSI_CYAN;
            }
            return (String.format(format,
                    new Date(lr.getMillis()),
                    lr.getLevel().getName(),
                    lr.getMessage(),
                    lr.getLoggerName()
            )+ANSI_RESET).replaceAll("-s-",s);
        }

    };
    static{
        Logger.getLogger("").getHandlers()[0].setFormatter(handler);
    }
    private Logger getLogger(){
        return Logger.getLogger(this.getClass().getSimpleName());
    };
    protected Logger classLogger = getLogger();
}
