package fr.matthis974.herobot.core_features.utils;

public abstract class LoadingObject extends Loggable implements StateLoadingObject {
    public void preLoadObject(){
        try {
            this.preEnable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
    public void loadObject(){
        try {
            this.enable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
    public void postLoadObject(){
        try {
            this.postEnable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
    public void preUnloadObject(){
        try {
            this.preDisable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
    public void unloadObject(){
        try {
            this.disable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
    public void postUnloadObject(){
        try {
            this.postDisable();
        }catch (Throwable t){
            this.onError(t);
        }
    }
}
