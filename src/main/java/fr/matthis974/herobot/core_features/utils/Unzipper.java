package fr.matthis974.herobot.core_features.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.UUID;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Unzipper extends Loggable {
    public void extractFolder(String zipFile, String extractFolder)
    {
        try
        {
            int BUFFER = 2048;
            URL website = new URL(zipFile);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            String u = UUID.randomUUID().toString();
            FileOutputStream fos = new FileOutputStream("./"+u+".zip");
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            ZipFile zip = new ZipFile("./"+u+".zip");

            new File(extractFolder).mkdir();
            Enumeration zipFileEntries = zip.entries();

            // Process each entry
            while (zipFileEntries.hasMoreElements())
            {
                // grab a zip file entry
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
                String currentEntry = entry.getName();

                File destFile = new File(extractFolder, currentEntry);
                //destFile = new File(newPath, destFile.getName());
                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                destinationParent.mkdirs();

                if (!entry.isDirectory())
                {
                    BufferedInputStream is = new BufferedInputStream(zip
                            .getInputStream(entry));
                    int currentByte;
                    // establish buffer for writing file
                    byte data[] = new byte[BUFFER];

                    // write the current file to disk
                    FileOutputStream fos2 = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(fos2,
                            BUFFER);

                    // read and write until last byte is encountered
                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }


            }
        }
        catch (Exception e)
        {
            classLogger.log(Level.WARNING,"Error while unzipping",e);
        }

    }
}
