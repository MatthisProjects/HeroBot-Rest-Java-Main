package fr.matthis974.herobot.core_features.utils;

import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;

public interface StateLoadingObject {
    void preEnable() throws Throwable;
    void enable() throws Throwable;
    void postEnable() throws Throwable;
    void preDisable() throws Throwable;
    void disable() throws Throwable;
    void postDisable() throws Throwable;
    void onError(Throwable throwable);
}
