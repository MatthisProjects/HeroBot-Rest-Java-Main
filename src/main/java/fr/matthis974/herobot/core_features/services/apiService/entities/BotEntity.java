package fr.matthis974.herobot.core_features.services.apiService.entities;

public class BotEntity {
    private String token;
    private String name;

    public String getGame() {
        return game;
    }

    private String game;
    public BotEntity(String token, String name,String game) {
        this.token = token;
        this.name = name;
        this.game = game;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        String tt = token;
        token = null;
        return tt;
    }
}
