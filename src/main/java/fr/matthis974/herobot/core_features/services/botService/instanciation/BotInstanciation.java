package fr.matthis974.herobot.core_features.services.botService.instanciation;

import fr.matthis974.herobot.core_features.services.apiService.entities.BotEntity;
import fr.matthis974.herobot.core_features.services.botService.BotManagerService;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import fr.matthis974.herobot.core_features.utils.LoadingObject;
import fr.matthis974.herobot.core_features.utils.StateLoadingObject;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.hooks.IEventManager;

import javax.security.auth.login.LoginException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;

public class BotInstanciation extends LoadingObject {
    private final ServicesManager sm;
    private JDA jda;
    private final BotEntity botEntity;
    public Object getJDA() {
        return jda;
    }
    public BotInstanciation(BotEntity entity, ServicesManager bms){
        this.botEntity = entity;
        this.sm = bms;
    }

    @Override
    public void preEnable() {

    }

    @Override
    public void enable() {
        try {
            jda = new JDABuilder(AccountType.BOT).setStatus(OnlineStatus.DO_NOT_DISTURB).setToken(botEntity.getToken()).setGame(Game.of(Game.GameType.STREAMING,botEntity.getGame(),"https://www.twitch.tv/herobotdiscord"))
                    .setEventManager((IEventManager) sm.callService("eventManager")).build();
        } catch (LoginException  | ServiceNotFoundException e) {
            classLogger.log(Level.WARNING,"Exception while loading some bots !",e);
            e.printStackTrace();
        }
    }

    @Override
    public void postEnable() {

    }

    @Override
    public void preDisable() {

    }

    @Override
    public void disable() {
        jda.shutdownNow();
        jda = null;
    }

    @Override
    public void postDisable() {

    }

    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }
}
