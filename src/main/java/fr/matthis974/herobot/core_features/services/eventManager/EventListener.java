package fr.matthis974.herobot.core_features.services.eventManager;

import net.dv8tion.jda.core.events.Event;

public interface EventListener {
    public boolean onEvent(final Event event);
}
