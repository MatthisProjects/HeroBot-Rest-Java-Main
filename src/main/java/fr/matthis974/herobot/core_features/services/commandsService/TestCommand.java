package fr.matthis974.herobot.core_features.services.commandsService;

import fr.matthis974.herobot.core_features.services.eventWaiter.EventWaiterService;
import fr.matthis974.herobot.core_features.services.eventWaiter.Executable;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;

public class TestCommand {
    @SuppressWarnings("unused")
    @Command(name="createNews", description="Creer une news")
    public void newsCommand(Message msg, TextChannel textChannel, User user) {
        textChannel.sendMessage("**Setup creation news !** Entrez le nom de la news :").complete();
        EventWaiterService eventListener = EventWaiterService.INSTANCE;
        eventListener.waitFor(new Executable() {
            @Override
            public boolean checkIsValid(Event event) {
                if(event instanceof GuildMessageReceivedEvent){
                    if(((GuildMessageReceivedEvent) event).getAuthor().getId().equalsIgnoreCase(user.getId()))
                    {
                        if(!msg.getContentRaw().equalsIgnoreCase(((GuildMessageReceivedEvent) event).getMessage().getContentRaw())){
                            return true;
                        }

                    }

                }
                return false;
            }

            @Override
            public void execute(Event event) {
                textChannel.sendMessage("**Entrez le contenu de la news pour envoyer le message :**").complete();
                GuildMessageReceivedEvent event1 = (GuildMessageReceivedEvent) event;
                String title = event1.getMessage().getContentRaw();
                eventListener.waitFor(new Executable() {
                    @Override
                    public boolean checkIsValid(Event event) {
                        if(event instanceof GuildMessageReceivedEvent){
                            if(((GuildMessageReceivedEvent) event).getAuthor().getId().equalsIgnoreCase(user.getId()))
                            {
                                if(!msg.getContentRaw().equalsIgnoreCase(((GuildMessageReceivedEvent) event).getMessage().getContentRaw())){
                                    return true;
                                }

                            }

                        }
                        return false;
                    }

                    @Override
                    public void execute(Event event) {
                        GuildMessageReceivedEvent event1 = (GuildMessageReceivedEvent) event;
                        String content = event1.getMessage().getContentRaw();
                        EmbedBuilder emb = new EmbedBuilder();
                        emb.setTitle(title);
                        emb.setColor(Color.BLUE);
                        emb.setAuthor(user.getName(),user.getAvatarUrl(),user.getAvatarUrl());
                        emb.setDescription(content);
                        emb.setFooter("Hero Bot v1.0 for "+textChannel.getGuild().getName(),textChannel.getGuild().getIconUrl());
                        textChannel.sendMessage("**Mention du channel et mention du role a mentionner ! :**").complete();
                        eventListener.waitFor(new Executable() {
                            @Override
                            public boolean checkIsValid(Event event) {
                                if(event instanceof GuildMessageReceivedEvent){
                                    if(((GuildMessageReceivedEvent) event).getAuthor().getId().equalsIgnoreCase(user.getId()))
                                    {
                                        if(!msg.getContentRaw().equalsIgnoreCase(((GuildMessageReceivedEvent) event).getMessage().getContentRaw())){
                                            return true;
                                        }

                                    }

                                }
                                return false;
                            }

                            @Override
                            public void execute(Event event) {
                                GuildMessageReceivedEvent event1 = (GuildMessageReceivedEvent) event;
                                TextChannel tc = event1.getMessage().getMentionedChannels().get(0);
                                Role r = event1.getMessage().getMentionedRoles().get(0);
                                tc.sendMessage(r.getAsMention()).complete().editMessage(emb.build()).complete();
                            }
                        });
                    }
                });
            }
        });
    }
    @SuppressWarnings("unused")
    @Command(name="ping", description="Ping pong ?")
    public void ping(TextChannel textChannel, User user) {
        textChannel.sendMessage("**Pong mon ami "+user.getAsMention()+"**").complete();
    }
}