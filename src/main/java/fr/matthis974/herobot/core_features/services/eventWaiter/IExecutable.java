package fr.matthis974.herobot.core_features.services.eventWaiter;

import net.dv8tion.jda.core.events.Event;

public interface IExecutable {
    boolean checkIsValid(Event event);
    void execute(Event event);
}
