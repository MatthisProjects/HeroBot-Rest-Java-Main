package fr.matthis974.herobot.core_features.services.eventManager;

import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.hooks.IEventManager;
import java.util.ArrayList;
import java.util.List;

public class EventManager extends Service implements IEventManager{

    private List<Object> registered = new ArrayList<>();

    public EventManager(ServicesManager pms) {
        super(pms);
    }

    @Override
    public void register(Object o) {
        if(o instanceof fr.matthis974.herobot.core_features.services.eventManager.EventListener){
            registered.add(o);
        }
    }

    @Override
    public void unregister(Object o) {
        registered.remove(o);
    }

    @Override
    public void handle(Event event) {
        boolean isCancelled = false;
        for (Object e:
             registered) {
            if(!isCancelled) {
                EventListener u = (fr.matthis974.herobot.core_features.services.eventManager.EventListener) e;
                isCancelled = u.onEvent(event);
            }
        }
    }

    @Override
    public List<Object> getRegisteredListeners() {
        return registered;
    }

    @Override
    public Object getService(Object... objects) {
        return this;
    }

    @Override
    public void enable() {
        classLogger.info("Hey im the Event Manager ! I help you to manage events !");
    }

    @Override
    public void disable(){

    }

    @Override
    public void onError(Throwable throwable) {

    }
}
