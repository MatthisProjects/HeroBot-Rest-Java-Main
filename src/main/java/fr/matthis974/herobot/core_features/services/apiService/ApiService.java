package fr.matthis974.herobot.core_features.services.apiService;

import fr.matthis974.herobot.core_features.services.apiService.entities.BotEntity;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.ServicesManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ApiService extends Service {
    public ApiService(ServicesManager pms) {
        super(pms);
    }

    @Override
    public Object getService(Object... objects) {
        this.classLogger.info("The api service has been called !");
        return this;
    }

    @Override
    public void enable() {
        this.classLogger.info("Loading Alive API !");
    }

    @Override
    public void postEnable() {
        this.classLogger.info("Successful !");
    }

    @Override
    public void disable() {

    }

    @Override
    public void onError(Throwable throwable) {

    }

    public List<BotEntity> getBotsForThisInstance(){
        return Arrays.asList(new BotEntity("NDg2NTUxMjA2MjkxMDQ2NDEw.DnAv5Q.0D6z5W__O5eUL9j_iXwmi2CFAuw", "Alive Bot 1.0", "HEROBOT"), new BotEntity("NDkxNjczNDgwMDA2MjA1NDYx.DoLSPQ.VRVxfzDvuRoXIcI8KPIvzywFZrM", "HeroBot Main Instance", "HEROBOT"));
    }
}
