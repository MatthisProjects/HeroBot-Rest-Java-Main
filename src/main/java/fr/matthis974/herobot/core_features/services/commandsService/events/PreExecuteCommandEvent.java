package fr.matthis974.herobot.core_features.services.commandsService.events;

import fr.matthis974.herobot.core_features.services.commandsService.Cancellable;
import net.dv8tion.jda.core.JDA;

public class PreExecuteCommandEvent extends Cancellable {
    public PreExecuteCommandEvent(JDA api) {
        super(api);
    }
}
