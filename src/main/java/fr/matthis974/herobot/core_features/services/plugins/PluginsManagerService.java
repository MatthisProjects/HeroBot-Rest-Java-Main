package fr.matthis974.herobot.core_features.services.plugins;

import fr.matthis974.herobot.core_features.services.apiService.entities.PluginEntity;
import fr.matthis974.herobot.core_features.system.PluginLoadRequest;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import org.xeustechnologies.jcl.JarClassLoader;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class PluginsManagerService extends Service {
    private JarClassLoader jarClassLoader = new JarClassLoader();
    private ArrayList<Object[]> instancesOfPlugins = new ArrayList<Object[]>();
    private Map<String,Double> plugins = new HashMap<>();
    public PluginsManagerService(ServicesManager pms) {
        super(pms);
    }


    @Override
    public Object getService(Object... objects) {
        return this;
    }

    @Override
    public void enable() throws InvocationTargetException, NoSuchMethodException, ServiceAlreadyExistsException, InstantiationException, IllegalAccessException, IOException, ClassNotFoundException {
        try{classLogger.info("Loading some Plugins");
        loadPlugin(new PluginEntity("https://localhost:80/dd2.jar"));
        loadPlugin(new PluginEntity("https://localhost:80/DebugMode.jar"));
        List<PluginLoadRequest> pluginLoadRequests = new ArrayList<>();
        for (Object[] jp:
             this.instancesOfPlugins) {
            pluginLoadRequests.add(new PluginLoadRequest((Class) jp[2],(Double) jp[1],(String) jp[0]));
        }
        this.pluginsManager.loadPluginsServices(pluginLoadRequests);
        classLogger.info("Finished loading some Plugins");}catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void disable() {
            
    }

    @Override
    public void onError(Throwable throwable) {

    }

    public JarClassLoader getJarClassLoader() {
        return jarClassLoader;
    }


    private void loadPlugin(PluginEntity entity) throws IOException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException {
        this.instancesOfPlugins.add(getClasseNames(downloadFromUrl(new URL(entity.getURL()),"download"+UUID.randomUUID().toString()).getPath())) ;
    }

    private Object[] getClasseNames(String jarName) throws IOException, ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ArrayList<String> classes = new ArrayList<>();
            jarClassLoader.add(new FileInputStream(
                    jarName));
            JarInputStream jarFile = new JarInputStream(new FileInputStream(
                    jarName));
            JarEntry jarEntry;
            Attributes att = jarFile.getManifest().getMainAttributes();
            String name = att.getValue("Plugin-Name");
            double version = Double.parseDouble(att.getValue("Plugin-Version"));
            String classPath = att.getValue("Plugin-ClassPath");
            while (true) {
                jarEntry = jarFile.getNextJarEntry();
                if (jarEntry == null) {
                    break;
                }
                if (jarEntry.getName().endsWith(".class")) {
                    classes.add(jarEntry.getName().replaceAll("/", "\\.").replaceAll(".class",""));
                }
            }
            for (String s:
                 classes) {
                jarClassLoader.loadClass(s);

            }
            this.plugins.put(name,version);
        return  new Object[]{name,version,jarClassLoader.getLoadedClasses().get(classPath)};

    }

    @SuppressWarnings("unused")
    private void extractFolder(String zipFile, String extractFolder) {
        try {
            int BUFFER = 2048;
            URL website = new URL(zipFile);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            String u = UUID.randomUUID().toString();
            FileOutputStream fos = new FileOutputStream("./" + u + ".zip");
            fos.getChannel().transferFrom(rbc, 0L, Long.MAX_VALUE);
            ZipFile zip = new ZipFile("./" + u + ".zip");
            if(!new File(extractFolder).mkdir()) throw new Exception("Can't create folder");
            Enumeration<? extends ZipEntry> zipFileEntries = zip.entries();
            while (zipFileEntries.hasMoreElements()) {
                int currentByte;
                ZipEntry entry = zipFileEntries.nextElement();
                String currentEntry = entry.getName();
                File destFile = new File(extractFolder, currentEntry);
                File destinationParent = destFile.getParentFile();
                if(!new File(extractFolder).mkdir()) throw new Exception("Can't create folder");
                if (entry.isDirectory()) continue;
                BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
                byte[] data = new byte[BUFFER];
                FileOutputStream fos2 = new FileOutputStream(destFile);
                BufferedOutputStream dest = new BufferedOutputStream(fos2, BUFFER);
                while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                    dest.write(data, 0, currentByte);
                }
                dest.flush();
                dest.close();
                is.close();
            }
        }
        catch (Exception e) {
            classLogger.log(Level.WARNING,"An exception has been detected while unzipping a file",e);
        }
    }


    private static File downloadFromUrl(URL url, String localFilename) throws IOException {
        InputStream is = null;
        FileOutputStream fos = null;

        String tempDir = System.getProperty("java.io.tmpdir");
        String outputPath = tempDir + "/" + localFilename;

        try {
            //connect
            URLConnection urlConn = url.openConnection();

            //get inputstream from connection
            is = urlConn.getInputStream();
            fos = new FileOutputStream(outputPath);

            // 4KB buffer
            byte[] buffer = new byte[4096];
            int length;

            // read from source and write into local file
            while ((length = is.read(buffer)) > 0) {

                fos.write(buffer, 0, length);
            }
            return new File(outputPath);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } finally {
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }

    public Map<String, Double> getPlugins() {
        return plugins;
    }
}
