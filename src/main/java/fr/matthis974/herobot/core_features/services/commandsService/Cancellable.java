package fr.matthis974.herobot.core_features.services.commandsService;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.events.Event;

public class Cancellable extends Event {
    private boolean cancelled = false;

    public Cancellable(JDA api) {
        super(api);
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isCancelled() {
        return cancelled;
    }
}
