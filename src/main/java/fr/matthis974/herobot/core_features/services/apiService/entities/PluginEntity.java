package fr.matthis974.herobot.core_features.services.apiService.entities;

public class PluginEntity {
    private String URL;

    public PluginEntity(String URL) {
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }
}
