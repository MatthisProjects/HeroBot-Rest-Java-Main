package fr.matthis974.herobot.core_features.services.commandsService;


import fr.matthis974.herobot.core_features.services.eventManager.EventListener;
import fr.matthis974.herobot.core_features.services.eventManager.EventManager;
import fr.matthis974.herobot.core_features.services.eventWaiter.EventWaiterService;
import fr.matthis974.herobot.core_features.services.plugins.PluginsManagerService;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;

import java.awt.*;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public final class CommandMap extends Service implements EventListener {


    private final Map<String, SimpleCommand> commands = new HashMap<>();
    private final String tag;
    private final EventWaiterService eventWaiterService;

    public CommandMap(ServicesManager sm){
        super(sm);
        this.eventWaiterService = EventWaiterService.INSTANCE;
        this.tag = "=";
        registerCommands(new TestCommand(),this);

    }
    @SuppressWarnings("unused")
    @Command(name="help", description="Affiche l'aide")
    private void help(User user, MessageChannel channel, JDA jda) throws ServiceNotFoundException {
        EmbedBuilder emb = new EmbedBuilder();
        emb.setTitle("Aide - " + jda.getSelfUser().getName()).setColor(Color.getHSBColor(255.27F,78.24F,84.71F));
        emb.addField("**General information**", "**Prefix :** `" + this.tag + "`", false);
        emb.addField("**Description du bot**","```markdown\r <HeroBot>\r HeroBot est un projet Alive Creations ! C'est un bot public impliquant des plugins que VOUS pouvez coder, des fonctionalitées poussées ect...```",false);
        this.commands.forEach((k, v) -> emb.addField("__***" + v.getName() + "***__",  "\r**Description :** " + v.getDescription(), true));
        emb.addField("**Plugins Chargés :**","```Comme ce bot imbrique des plugins voila la liste des plugins chargés (sert au débug)```",false);
        ((PluginsManagerService)this.pluginsManager.callService("pluginsManager")).getPlugins().forEach((k,v)->emb.addField("**"+k+"**","**Version :** "+v,true));
        user.openPrivateChannel().complete().sendMessage(emb.build()).queue();
        channel.sendMessage(user.getAsMention() + " je t'ai envoy\u00e9 l'aide en priv\u00e9 !").complete().delete().queueAfter(3L, TimeUnit.SECONDS);
    }
    @Override
    public boolean onEvent(Event event) {
        if (event instanceof GuildMessageReceivedEvent) {
            this.onMessage((GuildMessageReceivedEvent)event);
        }
        return false;
    }

    private void onMessage(GuildMessageReceivedEvent event) {
        if (Objects.equals(event.getAuthor(), event.getJDA().getSelfUser())) {
            return;
        }
        String message = event.getMessage().getContentRaw();
        if (message.startsWith(this.getTag())) {
            if (this.commandUser( message.replaceFirst(this.getTag(), ""), event.getMessage(),event.getJDA())) {
                event.getMessage().addReaction("\ud83c\udd97").complete();
            } else {
                event.getMessage().addReaction("\u274c").complete();
            }
        }
    }
    private String getTag() {
        return tag;
    }

    public void registerCommands(Object... objects){
        for(Object object : objects) registerCommand(object);
    }

    public void registerCommand(Object object){
        for(Method method : object.getClass().getDeclaredMethods()){
            if(method.isAnnotationPresent(Command.class)){
                Command command = method.getAnnotation(Command.class);
                method.setAccessible(true);
                SimpleCommand simpleCommand = new SimpleCommand(command.name(), command.description(), object, method, command.cost());
                commands.put(command.name(), simpleCommand);
            }
        }
    }

    private boolean commandUser(String command, Message message, JDA bi){
        Object[] object = getCommand(command);
        if(object[0] == null) return false;

        try{
            execute(((SimpleCommand)object[0]), command,(String[])object[1], message,bi);
        }catch(Exception exception){
            System.out.println("La methode "+((SimpleCommand)object[0]).getMethod().getName()+" n'est pas correctement initialisé.");
        }
        return true;
    }

    private Object[] getCommand(String command){
        String[] commandSplit = command.split(" ");
        String[] args = new String[commandSplit.length-1];
        System.arraycopy(commandSplit, 1, args, 0, commandSplit.length - 1);
        SimpleCommand simpleCommand = commands.get(commandSplit[0]);
        return new Object[]{simpleCommand, args};
    }

    private void execute(SimpleCommand simpleCommand, String command, String[] args, Message message,JDA bi) throws Exception{
        Parameter[] parameters = simpleCommand.getMethod().getParameters();
        Object[] objects = new Object[parameters.length];
        for(int i = 0; i < parameters.length; i++){
            if(parameters[i].getType() == String[].class) objects[i] = args;
            else if(parameters[i].getType() == User.class) objects[i] = message == null ? null : message.getAuthor();
            else if(parameters[i].getType() == TextChannel.class) objects[i] = message == null ? null : message.getTextChannel();
            else if(parameters[i].getType() == PrivateChannel.class) objects[i] = message == null ? null : message.getPrivateChannel();
            else if(parameters[i].getType() == Guild.class) objects[i] = message == null ? null : message.getGuild();
            else if(parameters[i].getType() == String.class) objects[i] = command;
            else if(parameters[i].getType() == Message.class) objects[i] = message;
            else if(parameters[i].getType() == JDA.class) objects[i] = bi;
            else if(parameters[i].getType() == EventWaiterService.class) objects[i] = eventWaiterService;
            else if(parameters[i].getType() == MessageChannel.class) objects[i] = message == null ? null : message.getChannel();
        }
        simpleCommand.getMethod().invoke(simpleCommand.getObject(), objects);
    }

    @Override
    public Object getService(Object... objects) {
        return this;
    }

    @Override
    public void enable() throws ServiceNotFoundException {
        EventManager eventManager = (EventManager)pluginsManager.callService("eventManager");
        eventManager.register(this);
    }

    @Override
    public void postEnable() {

    }


    @Override
    public void disable() {

    }

    @Override
    public void onError(Throwable throwable) {

    }
}