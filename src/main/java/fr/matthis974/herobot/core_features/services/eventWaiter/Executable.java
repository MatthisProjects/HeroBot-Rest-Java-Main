package fr.matthis974.herobot.core_features.services.eventWaiter;

public abstract class Executable implements IExecutable {
    boolean isCancelled = false;
    @SuppressWarnings("unused")
    protected void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }
}
