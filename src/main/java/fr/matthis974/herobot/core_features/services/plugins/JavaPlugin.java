package fr.matthis974.herobot.core_features.services.plugins;

import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.ServicesManager;

import java.util.Map;


public abstract class JavaPlugin extends Service {
    private final double version;
    private final String name;

    public JavaPlugin(ServicesManager manager, double version, String name) {
        super(manager);
        this.version = version;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof JavaPlugin){
            return ((JavaPlugin)obj).getName().equals(this.getName());
        }
        return false;
    }

    public String getName(){
        return name;
    }
    public double getVersion(){
        return version;
    }
}
