package fr.matthis974.herobot.core_features.services.mainSystem;

import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import fr.matthis974.herobot.core_features.services.plugins.PluginsManagerService;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;


public class MainSystemService extends Service {
    private loadThread libsLoadThread = new loadThread(this.pluginsManager);
    public MainSystemService(ServicesManager pms) {
        super(pms);
    }

    @Override
    public Object getService(Object... objects) {
        return this;
    }

    @Override
    public void enable() {
        this.classLogger.info("Hey, i am the mainSystem ! I'm loading libraries async !");
    }

    @Override
    public void postEnable() {
        libsLoadThread.run();
    }

    @Override
    public void disable() {

    }

    @Override
    public void onError(Throwable throwable) {

    }


    class loadThread implements Runnable{
        ServicesManager pluginsManagerService;

        loadThread(ServicesManager pluginsManager) {
            this.pluginsManagerService = pluginsManager;
        }
        boolean end = false;
        @Override
        public void run() {
            PluginsManagerService pluginsManagerService;
            try {
                pluginsManagerService = (PluginsManagerService) pluginsManager.callService("pluginsManager");
                pluginsManagerService.getJarClassLoader().add("libs/");
                for (File f:
                        Objects.requireNonNull(new File("libs/").listFiles())) {
                    for (Object s:
                            getClasseNames(f.getPath())
                    ) {
                        pluginsManagerService.getJarClassLoader().loadClass((String)s);
                    }
                }

                System.out.println("Loaded all libs ! with "+pluginsManagerService.getJarClassLoader().getLoadedClasses().size()+" classes !");

            } catch (ServiceNotFoundException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            end = true;
        }
    }

    private static List getClasseNames(String jarName) {
        ArrayList<String> classes = new ArrayList<>();
        try {
            JarInputStream jarFile = new JarInputStream(new FileInputStream(
                    jarName));
            JarEntry jarEntry;

            while (true) {
                jarEntry = jarFile.getNextJarEntry();
                if (jarEntry == null) {
                    break;
                }
                if (jarEntry.getName().endsWith(".class")) {
                    classes.add(jarEntry.getName().replaceAll("/", "\\.").replaceAll(".class",""));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classes;

    }

}
