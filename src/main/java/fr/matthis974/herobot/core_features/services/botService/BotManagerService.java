package fr.matthis974.herobot.core_features.services.botService;

import fr.matthis974.herobot.core_features.services.apiService.ApiService;
import fr.matthis974.herobot.core_features.services.apiService.entities.BotEntity;
import fr.matthis974.herobot.core_features.services.botService.instanciation.BotInstanciation;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import fr.matthis974.herobot.core_features.system.Service;
import net.dv8tion.jda.core.hooks.InterfacedEventManager;

import java.util.ArrayList;
import java.util.List;

public class BotManagerService extends Service {
    private List<BotInstanciation> botEntityList = new ArrayList<>();
    private ApiService apiService;
    private InterfacedEventManager o = new InterfacedEventManager();

    public InterfacedEventManager getO() {
        return o;
    }

    public BotManagerService(ServicesManager pms) {
        super(pms);
    }

    @Override
    public void enable() {
        this.classLogger.info("Trying to call service Bot Manager !");
        try {
            this.apiService = (ApiService) this.pluginsManager.callService("apiService");
        } catch (ServiceNotFoundException e) {
            e.printStackTrace();
        }
        List<BotEntity> array= apiService.getBotsForThisInstance();
        for (BotEntity s:
                array) {
            this.classLogger.info("Loading bot "+s.getName());
            BotInstanciation b = new BotInstanciation(s,this.pluginsManager);
            botEntityList.add(b);
        }

        for (BotInstanciation b:
                botEntityList) {
            b.preLoadObject();
        }
        for (BotInstanciation b:
                botEntityList) {
            b.loadObject();
        }
        for (BotInstanciation b:
                botEntityList) {
            b.postLoadObject();
        }
    }



    @Override
    public void disable(){
        for (BotInstanciation b:
                botEntityList) {
            b.preUnloadObject();
        }
        for (BotInstanciation b:
                botEntityList) {
            b.unloadObject();
        }
        for (BotInstanciation b:
                botEntityList) {
            b.postUnloadObject();
        }
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public Object getService(Object... objects) {
        return this;
    }

    public List<BotInstanciation> getBotEntityList() {
        return botEntityList;
    }
}
