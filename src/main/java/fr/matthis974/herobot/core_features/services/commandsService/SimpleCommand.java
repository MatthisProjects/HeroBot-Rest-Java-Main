package fr.matthis974.herobot.core_features.services.commandsService;

import java.lang.reflect.Method;



public final class SimpleCommand {

    private final String name, description;

    private final Object object;
    private final Method method;
    private final int power;

    SimpleCommand(String name, String description, Object object, Method method, int power){
        super();
        this.name = name;
        this.description = description;

        this.object = object;
        this.method = method;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }



    public Object getObject() {
        return object;
    }

    public Method getMethod() {
        return method;
    }

    public int getPower() {
        return power;
    }
}