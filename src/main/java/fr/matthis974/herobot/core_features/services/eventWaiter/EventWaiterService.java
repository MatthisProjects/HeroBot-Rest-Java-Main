package fr.matthis974.herobot.core_features.services.eventWaiter;

import fr.matthis974.herobot.core_features.services.botService.BotManagerService;
import fr.matthis974.herobot.core_features.services.botService.instanciation.BotInstanciation;
import fr.matthis974.herobot.core_features.services.eventManager.EventListener;
import fr.matthis974.herobot.core_features.services.eventManager.EventManager;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.events.Event;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;

public class EventWaiterService extends Service implements EventListener {
    private CopyOnWriteArrayList<Executable> waitingFor = new CopyOnWriteArrayList<>();
    public static EventWaiterService INSTANCE;
    public EventWaiterService(ServicesManager pms) {
        super(pms);
        INSTANCE = this;
    }


    @Override
    public boolean onEvent(Event event) {
        List<Executable> toDelete = new ArrayList<>();
        boolean isCancelled = false;
        for (Executable e :
                waitingFor) {
            if (e.checkIsValid(event) && !isCancelled) {
                try {
                    e.execute(event);
                    toDelete.add(e);
                    isCancelled = e.isCancelled;
                }catch (Exception ee){
                    classLogger.log(Level.WARNING,"Error",ee);
                }
            }

        }
        waitingFor.removeAll(toDelete);
        return false;
    }
    public void waitFor(Executable checkInterface) {
        this.waitingFor.add(checkInterface);
    }
    @Override
    public Object getService(Object... objects) {
        return this;
    }


    @Override
    public void enable()  {

    }

    @Override
    public void postEnable() throws Throwable {
        EventManager eventManager = (EventManager)pluginsManager.callService("eventManager");
        eventManager.register(this);
    }

    @Override
    public void disable(){

    }

    @Override
    public void onError(Throwable throwable) {

    }
}
