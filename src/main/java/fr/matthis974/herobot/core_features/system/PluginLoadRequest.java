package fr.matthis974.herobot.core_features.system;

public class PluginLoadRequest {
    private final Class clazz;
    private final double version;
    private final String name;

    public Class getClazz() {
        return clazz;
    }

    public double getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public PluginLoadRequest(Class clazz, double version, String name) {
        this.clazz = clazz;
        this.version = version;
        this.name = name;
    }
}
