package fr.matthis974.herobot.core_features.system;

import fr.matthis974.herobot.core_features.utils.StateLoadingObject;

public interface IService extends StateLoadingObject {
    Object getService(Object... objects);
}
