package fr.matthis974.herobot.core_features.system;

import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;
import fr.matthis974.herobot.core_features.utils.Loggable;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServicesManager extends Loggable {

    private final Map<String, Service> services;
    private final Map<String,Class> classList;
    private final Map<Class,Object> possibleArgs;
    public ServicesManager(Map<String, Class> basicServices) {
        this.services = new HashMap<>();
        possibleArgs = new HashMap<>();
        possibleArgs.put(ServicesManager.class,this);
        classList = basicServices;
        try {
            this.loadAllServices();
        } catch (InvocationTargetException | NoSuchMethodException | ServiceAlreadyExistsException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
    public void addClass(String s, Class c){
        classList.put(s,c);
    }
    public Object callService(String serviceName,Object... arguments) throws ServiceNotFoundException {
        if(!services.containsKey(serviceName)) throw new ServiceNotFoundException();
        Service s = services.get(serviceName);
        return s.getService(arguments);
    }
    @SuppressWarnings("unchecked")
    public Service registerService(String s, Class service,Object ... initArgs) throws ServiceAlreadyExistsException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if(services.containsKey(s)) throw new ServiceAlreadyExistsException();
        Map<Class,Object> d = new HashMap<>(this.possibleArgs);
        for (Object o:
             initArgs) {
            if(o != null) {
                System.out.println("Added "+o.getClass());
                if (o.getClass() == Double.class){
                    d.put(double.class,o);
                }
                if (o.getClass() == Integer.class){
                    d.put(int.class,o);
                }
                d.put(o.getClass(), o);
            }
        }
        Class[] args = service.getConstructors()[0].getParameterTypes();
        Object[] obj = new Object[args.length];
        for (int i = 0;i != args.length;i++) {
            obj[i] = d.getOrDefault(args[i], null);
        }
        services.put(s,(Service) service.getConstructors()[0].newInstance(obj));


        return services.get(s);
    }
    private void loadAllServices() throws InvocationTargetException, NoSuchMethodException, ServiceAlreadyExistsException, InstantiationException, IllegalAccessException {
        this.services.clear();
        this.classLogger.info("Instantiating all services.");
        for (Map.Entry<String, Class> s:
             this.classList.entrySet()) {
            Service s2 = registerService(s.getKey(),s.getValue());
        }
        for (Map.Entry<String, Service> s:
             this.services.entrySet()) {
            s.getValue().preLoadObject();

        }
        for (Map.Entry<String, Service> s:
                this.services.entrySet()) {
            classLogger.info("Enabling service "+s.getKey());
            s.getValue().loadObject();
        }
        for (Map.Entry<String, Service> s:
                this.services.entrySet()) {
            s.getValue().postLoadObject();
        }
        this.classLogger.info("Finished loading all services !");
    }

    public void stop() {
        for (Map.Entry<String, Service> s:
                this.services.entrySet()) {
            s.getValue().preUnloadObject();
        }
        for (Map.Entry<String, Service> s:
                this.services.entrySet()) {
            s.getValue().unloadObject();
        }
        for (Map.Entry<String, Service> s:
                this.services.entrySet()) {
            s.getValue().postUnloadObject();
        }
        this.classLogger.info("Finished unloading all services !");
    }


    public void deleteService(String name) {
        this.classList.remove(name);
    }

    public void loadPluginsServices(List<PluginLoadRequest> a) throws InvocationTargetException, NoSuchMethodException, ServiceAlreadyExistsException, InstantiationException, IllegalAccessException {
        classLogger.info("Loading plugins services !");
        List<Service> list = new ArrayList<>();
        for (PluginLoadRequest s:
             a) {
            list.add(this.registerService(s.getName()+"v"+s.getVersion(),s.getClazz(),s.getVersion(),s.getName()));
        }

        for (Service s:
             list) {
            s.preLoadObject();
        }
        for (Service s:
                list) {
            s.loadObject();
        }
        for (Service s:
                list) {
            s.postLoadObject();
        }
    }
}
