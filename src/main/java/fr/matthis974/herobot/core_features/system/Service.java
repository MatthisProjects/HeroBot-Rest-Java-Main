package fr.matthis974.herobot.core_features.system;


import fr.matthis974.herobot.core_features.utils.LoadingObject;

public abstract class Service extends LoadingObject implements IService {
    protected ServicesManager pluginsManager;

    @Override
    public void postDisable() {

    }

    @Override
    public void postEnable() throws Throwable {

    }

    @Override
    public void preDisable() {

    }

    @Override
    public void preEnable() {

    }

    public Service(ServicesManager pms){
        this.pluginsManager = pms;
    }
}
