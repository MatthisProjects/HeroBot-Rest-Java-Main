package fr.matthis974.herobot;

import fr.matthis974.herobot.core_features.services.apiService.ApiService;
import fr.matthis974.herobot.core_features.services.botService.BotManagerService;
import fr.matthis974.herobot.core_features.services.commandsService.Command;
import fr.matthis974.herobot.core_features.services.commandsService.CommandMap;
import fr.matthis974.herobot.core_features.services.eventManager.EventManager;
import fr.matthis974.herobot.core_features.services.eventWaiter.EventWaiterService;
import fr.matthis974.herobot.core_features.services.mainSystem.MainSystemService;
import fr.matthis974.herobot.core_features.services.plugins.PluginsManagerService;
import fr.matthis974.herobot.core_features.system.Service;
import fr.matthis974.herobot.core_features.system.ServicesManager;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceAlreadyExistsException;
import fr.matthis974.herobot.core_features.system.exceptions.ServiceNotFoundException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Scanner;

public class BooterService extends Service implements Runnable{
    private static ServicesManager manager;
    private static Scanner sc = new Scanner(System.in);
    private static HashMap<String, Class> map = null;

    static{
        System.out.println(
                "\u001B[32m[\n"+
                "HHHHHHHHH     HHHHHHHHH                                                              BBBBBBBBBBBBBBBBB                             tttt          \n" +
                "H:::::::H     H:::::::H                                                              B::::::::::::::::B                         ttt:::t          \n" +
                "H:::::::H     H:::::::H                                                              B::::::BBBBBB:::::B                        t:::::t          \n" +
                "HH::::::H     H::::::HH                                                              BB:::::B     B:::::B                       t:::::t          \n" +
                "  H:::::H     H:::::H      eeeeeeeeeeee    rrrrr   rrrrrrrrr      ooooooooooo          B::::B     B:::::B   ooooooooooo   ttttttt:::::ttttttt    \n" +
                "  H:::::H     H:::::H    ee::::::::::::ee  r::::rrr:::::::::r   oo:::::::::::oo        B::::B     B:::::B oo:::::::::::oo t:::::::::::::::::t    \n" +
                "  H::::::HHHHH::::::H   e::::::eeeee:::::eer:::::::::::::::::r o:::::::::::::::o       B::::BBBBBB:::::B o:::::::::::::::ot:::::::::::::::::t    \n" +
                "  H:::::::::::::::::H  e::::::e     e:::::err::::::rrrrr::::::ro:::::ooooo:::::o       B:::::::::::::BB  o:::::ooooo:::::otttttt:::::::tttttt    \n" +
                "  H:::::::::::::::::H  e:::::::eeeee::::::e r:::::r     r:::::ro::::o     o::::o       B::::BBBBBB:::::B o::::o     o::::o      t:::::t          \n" +
                "  H::::::HHHHH::::::H  e:::::::::::::::::e  r:::::r     rrrrrrro::::o     o::::o       B::::B     B:::::Bo::::o     o::::o      t:::::t          \n" +
                "  H:::::H     H:::::H  e::::::eeeeeeeeeee   r:::::r            o::::o     o::::o       B::::B     B:::::Bo::::o     o::::o      t:::::t          \n" +
                "  H:::::H     H:::::H  e:::::::e            r:::::r            o::::o     o::::o       B::::B     B:::::Bo::::o     o::::o      t:::::t    tttttt\n" +
                "HH::::::H     H::::::HHe::::::::e           r:::::r            o:::::ooooo:::::o     BB:::::BBBBBB::::::Bo:::::ooooo:::::o      t::::::tttt:::::t\n" +
                "H:::::::H     H:::::::H e::::::::eeeeeeee   r:::::r            o:::::::::::::::o     B:::::::::::::::::B o:::::::::::::::o      tt::::::::::::::t\n" +
                "H:::::::H     H:::::::H  ee:::::::::::::e   r:::::r             oo:::::::::::oo      B::::::::::::::::B   oo:::::::::::oo         tt:::::::::::tt\n" +
                "HHHHHHHHH     HHHHHHHHH    eeeeeeeeeeeeee   rrrrrrr               ooooooooooo        BBBBBBBBBBBBBBBBB      ooooooooooo             ttttttttttt  \n]"
                );
        HashMap<String,Class> map = new HashMap<>();
        map.put("eventManager",EventManager.class);
        map.put("apiService", ApiService.class);
        map.put("botManager", BotManagerService.class);
        map.put("mainSystem", MainSystemService.class);
        map.put("mainBooter",BooterService.class);
        map.put("commandsSystem", CommandMap.class);
        map.put("eventWaiter", EventWaiterService.class);
        map.put("pluginsManager", PluginsManagerService.class);
        BooterService.map = map;
        BooterService.manager = new ServicesManager(map);
    }

    private boolean isRunning = true;

    public BooterService(ServicesManager pms) {
        super(pms);
    }

    @Override
    public Object getService(Object... objects) {
        return this;
    }

    @Override
    public void enable() throws InterruptedException, IOException, ClassNotFoundException, AWTException, UnsupportedAudioFileException, LineUnavailableException, URISyntaxException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, ServiceAlreadyExistsException, ServiceNotFoundException {

    }

    @Override
    public void postEnable() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {if(isRunning)manager.stop();}));
        new Thread(this).start();
    }

    @Override
    public void disable() {

    }



    @Override
    public void onError(Throwable throwable) {

    }

    public static void main(String ... args){

    }

    @Override
    public void run() {
        String s = "help";
        while (!s.equalsIgnoreCase("stop") && !s.equalsIgnoreCase("end") && !s.equalsIgnoreCase("finish")){
                if(sc.hasNext())s = sc.nextLine();
        }
        isRunning = false;
        sc.close();
        manager.stop();
        System.exit(0);
    }
}
