package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

public enum SanctionType {
    BLACKLIST(1),
    BAN(2),
    KICK(3),
    MUTE(4);

    int i;
    SanctionType(int i) {
        this.i = i;
    }

}
