package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

import java.time.LocalDateTime;

public class Warn {
    public int id;
    public String _date_time;
    public String reason;
    public Account victim;
    public Account executor;
    public int count;
}
