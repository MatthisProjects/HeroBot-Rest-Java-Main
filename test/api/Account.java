package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

import java.util.ArrayList;
import java.util.List;

public class Account {
        public int id;
        public String discord_account_id;
        public List<Sanction> sanctions = new ArrayList<>();
        public List<Warn> warns = new ArrayList<>();
        public List<Sanction> sanctions_donnes = new ArrayList<>();
        public List<Warn> warns_donnes = new ArrayList<>();
}
