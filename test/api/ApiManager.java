package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;
import okio.BufferedSink;
import okio.ByteString;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class ApiManager implements ApiInterface {

    public Gson gson;
    private OkHttpClient okHttp;
    public String prefix;
    @Override
    public void initApi() {
        okHttp = new OkHttpClient.Builder().readTimeout(10,TimeUnit.MINUTES).build();

        gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    }

    @Override
    public String connect(Request request) throws IOException {
        return Objects.requireNonNull(okHttp.newCall(request).execute().body()).string();


    }


    @Override
    public void addWarn(Warn warn, Account account,Account exe,int count) throws IOException {
        RequestBody requestBody = new RequestBody() {
            @Nullable
            @Override
            public MediaType contentType() {
                return MediaType.parse("application/json");
            }

            @Override
            public void writeTo(BufferedSink bufferedSink) throws IOException {
                bufferedSink.write(ByteString.encodeUtf8("{\n" +
                        "            \"reason\": \""+warn.reason+"\",\n" +
                        "            \"victim\": null,\n" +
                        "            \"executor\": null,\n" +
                        "            \"count\": "+count+"\n" +
                        "}"));
            }
        };
        Request req = new Request.Builder().url(prefix+"api/user/"+account.discord_account_id+"/warns/add?executor_id="+exe.id).post(requestBody).build();
        connect(req);
    }

    @Override
    public void addSanction(Sanction sanction, Account account,Account exe) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/"+account.discord_account_id+"/sanctions/add?executor_id="+exe.id).build();
        connect(req);
    }

    @Override
    public Account getAccount(String discordId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/"+discordId).build();
        return gson.fromJson(connect(req),Account.class);
    }

    @Override
    public void removeWarnById(String warnId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/warns/"+warnId+"/remove").build();
        connect(req);
    }

    @Override
    public void removeSanctionById(String sanctionId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/sanctions/"+sanctionId+"/remove").build();
        connect(req);
    }

    @Override
    public List<Warn> getJustWarnsOfUser(String userId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/"+userId+"/warns").build();
        return gson.fromJson(connect(req),new ArrayList<Warn>().getClass());
    }

    @Override
    public List<Sanction> getJustSanctionsOfUser(String userId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/"+userId+"/sanctions").build();
        return gson.fromJson(connect(req),new ArrayList<Sanction>().getClass());
    }

    public void addUser(Account account) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/create").post(new RequestBody() {
            @Nullable
            @Override
            public MediaType contentType() {
                return MediaType.parse("application/json");
            }

            @Override
            public void writeTo(BufferedSink bufferedSink) throws IOException {
                bufferedSink.write(ByteString.encodeUtf8(gson.toJson(account)));
            }
        }).build();
        connect(req);
    }

    public boolean userExists(String discordId) throws IOException {
        Request req = new Request.Builder().url(prefix+"api/user/exists/?discord="+discordId).get().build();
        Response rsp = okHttp.newCall(req).execute();
        return rsp.body().string().equalsIgnoreCase("yes");
    }
}
