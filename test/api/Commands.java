package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

import fr.fanaticstudio.matthis974jump.botsurveillancediscord.commandsSystem.Command;
import fr.fanaticstudio.matthis974jump.botsurveillancediscord.commandsSystem.Roles;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class Commands {
    @Command(name = "info",description = "Affiche les infos d'un user",cost = 0,permissionsLevel = Roles.ADMIN,aliasses = {})
    public void infoCommand(Message m) throws IOException {
        if(m.getMentionedMembers().size() > 0){
            Account a = MainSystem.apiManager.getAccount(m.getMentionedMembers().get(0).getUser().getId());
            EmbedBuilder emb = new EmbedBuilder();
            StringBuilder builderW = new StringBuilder();
            StringBuilder builderS = new StringBuilder();
            StringBuilder builderWD = new StringBuilder();
            StringBuilder builderSD = new StringBuilder();
            HashMap<String,List<Warn>> warn = new HashMap<>();
            HashMap<String,List<Sanction>> sanctions = new HashMap<>();
            HashMap<String,List<Warn>> warnD = new HashMap<>();
            HashMap<String,List<Sanction>> sanctionsD = new HashMap<>();
            for (Warn w:
                 a.warns) {
                if(w.executor == null){w.executor = w.victim;}

                builderW.append("**Warn : **").append(w.reason).append(" : *").append(w.count).append(" (fois)* par <@").append(w.executor.discord_account_id).append(">\r");
            }
            for (Warn w:
                    a.warns_donnes) {
                if(w.victim == null){w.victim = w.executor;}
                builderWD.append("Warn donné : ").append(w.reason).append(" a ").append("<@").append(w.victim.discord_account_id).append(">").append(" (").append(w.count).append(" foi(s) )\r");
            }
            for (Sanction s: a.sanctions){
                if(s.executor == null){s.executor = s.victim;}
                s.init();
                builderS.append("Sanction : ").append(s.typep).append(" pour ").append(s.reason).append(" par <@").append(s.executor.discord_account_id).append(">");
            }

            for (Sanction s: a.sanctions_donnes){
                if(s.victim == null){s.victim = s.executor;}
                s.init();
                builderSD.append("Danction Donné : ").append(s.typep).append(" pour ").append(s.reason).append(" a <@").append(s.victim.discord_account_id).append(">");
            }
            emb.setTitle("Compte de "+m.getMentionedMembers().get(0).getEffectiveName())
                    .addField("Warns",builderW.toString(),false)
                    .addField("Warns Donnés",builderWD.toString(),false)
                    .addField("Sanctions",builderS.toString(),false)
                    .addField("Sanctions Donnés",builderSD.toString(),false);

            m.getChannel().sendMessage(emb.build()).complete();
        }
    }
}
