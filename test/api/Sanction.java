package fr.fanaticstudio.matthis974jump.botsurveillancediscord.api;

import java.time.LocalDateTime;

public class Sanction {
        public int id;
        public String _date_time;
        public String reason;
        public Account victim;
        public Account executor;
        private int type;
        public SanctionType typep;
        public void init(){
                for (SanctionType s:SanctionType.values()
                     ) {
                        if(s.i == type){
                                typep = s;
                                break;
                        }
                }
        }
}
